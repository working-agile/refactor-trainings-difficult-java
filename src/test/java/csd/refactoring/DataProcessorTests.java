package csd.refactoring;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataProcessorTests {

	@Nested
	@DisplayName("Business rule: Super Early Bird discounts")
	class SuperEarlyBirdDiscounts {

		@DisplayName("Super Early Bird discount starts day 21 before training course")
		@Test
		void super_early_bird_discount_on_day_21_before_training_course() {

			// Arrange
			/*
				Days before training course:	21
				type:							CSD
				Full price (no discounts):		4000
				SuperEarlyBird discount:		-600
				-->
				discounted price:				3400

			 */
			Item i1 = new Item(21 + 1, 50, 20, true, "CSD", 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(3400, processedItems[0].price);
		}


		@DisplayName("Super Early Bird discount for CSD training course")
		@Test
		void super_Early_bird_discount_For_CSD() {

			// Arrange
			/*
				Days before training course:	25
				type:							CSM
				Full price (no discounts):		3000
				-->
				SuperEarlyBird discount:		 -600
				Resulting price:				2400
			 */

			// Arrange
			Item i1 = new Item(25, 50, 20, true, "CSD", 3000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(2400, processedItems[0].price);
		}

		@DisplayName("Super Early Bird discount for CSM training course")
		@Test
		void super_Early_bird_discount_For_CSM() {

			// Arrange
			/*
				Days before training course:	25
				type:							CSM
				Full price (no discounts):		3000
				-->
				SuperEarlyBird discount:		 -600
			 */
			Item i1 = new Item(25, 50, 20, true, "CSM", 3000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(2400, processedItems[0].price);
		}


		@DisplayName("Super Early Bird discount for CSPO training course")
		@Test
		void super_Early_bird_discount_For_CSPO() {

			// Arrange
			/*
				Days before training course:	25
				type:							CSPO
				Full price (no discounts):		3000
				-->
				SuperEarlyBird discount:		 -700
			 */
			Item i1 = new Item(32, 50, 20, true, "CSPO", 3000);
			Item[] items = new Item[]{i1};
			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(2300, processedItems[0].price);
		}

	}


	@Nested
	@DisplayName("Business rule: Minimum prices are guaranteed")
	class MinimumPrices {

		@DisplayName("Super Early Bird discount overruled to guarantee minimum price for CSD")
		@Test
		void super_early_bird_discount_overruled_by_minimum_price_for_CSD() {

			// Arrange
			/*
				Days before training course:	25
				type:							CSD
				Full price (no discounts):		1200
				Minimum price CSD:				900
				-->
				SuperEarlyBird discount:		 -600
			 */
			Item i1 = new Item(25+1, 50, 20, true, "CSD", 1200);
			Item[] items = new Item[] { i1 };
			int MINIMUM_PRICE_CSD = 900;

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(MINIMUM_PRICE_CSD, processedItems[0].price);
		}

		@DisplayName("Super Early Bird discount overruled to guarantee minimum price for CSM")
		@Test
		void super_early_bird_discount_overruled_by_minimum_price_for_CSM() {

			// Arrange
			/*
				Days before training course:	25
				type:							CSM
				Full price (no discounts):		1500
				Minimum price CSM:				1000
				-->
				SuperEarlyBird discount:		 -600
			 */
			Item i1 = new Item(25+1, 50, 20, true, "CSM", 1500);
			Item[] items = new Item[] { i1 };
			int MINIMUM_PRICE_CSM = 1000;

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(MINIMUM_PRICE_CSM, processedItems[0].price);
		}

		@DisplayName("Super Early Bird discount overruled to guarantee minimum price for CSPO")
		@Test
		void super_early_bird_discount_overruled_by_minimum_price_for_CSPO() {

			// Arrange
			/*
				Days before training course:	25
				type:							CSPO
				Full price (no discounts):		1500
				Minimum price CSM:				1000
				-->
				SuperEarlyBird discount:		 -700
			 */
			Item i1 = new Item(25+1, 50, 20, true, "CSPO", 1500);
			Item[] items = new Item[] { i1 };
			int MINIMUM_PRICE_CSPO = 1200;

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(MINIMUM_PRICE_CSPO, processedItems[0].price);
		}

	}



	@Nested
	@DisplayName("Business rule: Full price policy")
	class FullPricePolicy {

		@DisplayName("Full prices on the day of the training course")
		@ParameterizedTest
		@ValueSource(strings = {"CSD", "CSPO", "CSM"})
		void should_have_full_price_on_the_day_of_the_training_class(String trainingCourseType) {

			// Arrange
			/*
				Days before training course:	0
				type:							CSD, CSPO, CSM
				Full price (no discounts):		4000
				-->
				no discount:		 			4000
			 */
			Item i1 = new Item(0+1, 50, 20, true, trainingCourseType, 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(4000, processedItems[0].price);
		}

		@DisplayName("Full prices on the day prior to the training course")
		@ParameterizedTest
		@ValueSource(strings = {"CSD", "CSPO", "CSM"})
		void should_have_full_price_on_the_day_prior_to_the_training_class(String trainingCourseType) {

			// Arrange
			/*
				Days before training course:	1
				Full price (no discounts):		4000
				-->
				no discount:		 			4000
			 */
			Item i1 = new Item(1+1, 50, 20, true, trainingCourseType, 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(4000, processedItems[0].price);
		}


		@DisplayName("Full prices 5 days prior to the training course if 3 or less seats left")
		@ParameterizedTest
		@ValueSource(strings = {"CSD", "CSPO", "CSM"})
		void should_have_full_price_5_days_prior_to_the_training_class_if_3_or_less_seats_left(String trainingCourseType) {

			// Arrange
			/*
				Days before training course:	5
				Seats left:						2
				Full price (no discounts):		4000
				-->
				no discount:		 			4000
			 */
			Item i1 = new Item(5+1, 50, 2, true, trainingCourseType, 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(4000, processedItems[0].price);
		}

	}



	@Nested
	@DisplayName("Business rule: Proportional discount 10 days before the training course")
	class ProportionalDiscount {

		@DisplayName("Proportional discount when 6 days before the training course")
		@Test
		void should_apply_proportional_discount_from_day_6_for_CSD() {

			// Arrange
			/*
				Days before training course:	6
				Type:							CSD
				Full price:						4000
				Proportional discount:			6*30
				-->
				discounted price:		 		4000-(6*30) = 3820
			 */
			Item i1 = new Item(6+1, 50, 25, true, "CSD", 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(3820, processedItems[0].price, "proportional discount expected");
		}

		@DisplayName("Proportional discount when 5 days before the training course and enough seats available")
		@Test
		void should_apply_proportional_discount_day_5_for_CSD_when_enough_seats_available() {

			// Arrange
			/*
				Days before training course:	5
				Type:							CSD
				Full price:						4000
				Proportional discount:			5*30
				-->
				discounted:		 				4000-(5*30) = 3850
			 */
			Item i1 = new Item(5+1, 50, 25, true, "CSD", 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(3850, processedItems[0].price, "proportional discount expected when enough seats available");
		}



		@DisplayName("Proportional discount when 10 days before the training course")
		@ParameterizedTest
		@ValueSource(strings = {"CSPO", "CSM"})
		void should_apply_proportional_discount_for_CSM_and_CSPO(String trainingCourseType) {

			// Arrange
			/*
				Days before training course:	10
				Type:							CSPO e CSM
				Full price:						4000
				Proportional discount:			10*30
				-->
				discounted:		 				4000-(10*20) = 3800
			 */
			Item i1 = new Item(10+1, 50, 25, true, trainingCourseType, 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(3800, processedItems[0].price, "should apply proportional discount - first day of the interval");
		}

	}








	@Nested
	@DisplayName("Business rule: Early bird discount when more than 10 and up to 20 days before training course")
	class EarlyBirdDiscount {

		@DisplayName("Early Bird discount when more than 10 before CSD training course")
		@Test
		void start_applying_early_bird_discount_with_more_than_10_days_before_CSD_training_course() {

			// Arrange
			/*
				Days before training course:	11
				Type:							CSD
				Full price:						4000
				discount:						500
				-->
				discounted:		 				4000-500 = 3500
			 */
			Item i1 = new Item(11+1, 50, 25, true, "CSD", 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(3500, processedItems[0].price, "should apply early bird discount for CSD");
		}


		@DisplayName("Early Bird discount when 20 days before CSD training course")
		@Test
		void start_applying_early_bird_discount_when_20_days_before_CSD_training_course() {

			// Arrange
			/*
				Days before training course:	20
				Type:							CSD
				Full price:						4000
				discount:						500
				-->
				discounted:		 				4000-500 = 3500
			 */
			Item i1 = new Item(20+1, 50, 25, true, "CSD", 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(3500, processedItems[0].price, "should apply early bird discount for CSD");
		}



		@DisplayName("Early Bird discount when 15 days before CSM or CSPO training course")
		@ParameterizedTest
		@ValueSource(strings = {"CSPO", "CSM"})
		void start_applying_early_bird_discount_when_15_days_before_CSM_or_CSPO_training_course(String trainingCourseType) {

			// Arrange
			/*
				Days before training course:	15
				Type:							CSM or CSPO
				Full price:						4000
				discount:						400
				-->
				discounted:		 				4000-400 = 3600
			 */
			Item i1 = new Item(15+1, 50, 25, true, trainingCourseType, 4000);
			Item[] items = new Item[]{i1};

			// Act
			Item[] processedItems = DataProcessor.processData(items);

			// Assert
			assertEquals(3600, processedItems[0].price, "should apply early bird discount for " + trainingCourseType);
		}

	}



	@Nested
	@DisplayName("Business rule: Total remaining sales target")
	class TotalRemainingSalesTarget {

		@DisplayName("Should calculate the total remaining sales")
		@Test
		void should_calculate_total_value_of_remaining_training_courses() {

			/*
				Number of seats:				5
				Days before training course:	30
				Type:							CSD
				Full price:						4000
				super early bird discount:		 600
				Discounted price:				3400
			 */
			Item i1 = new Item(30+1, 50, 5, true, "CSD", 4000);
			/*
				Number of seats:				1
				Days before training course:	15
				Type:							CSD
				Full price:						4000
				Early bird discount:		 	500
				Discounted price:				3500
			 */
			Item i2 = new Item(15+1, 50, 1, true, "CSD", 4000);
			Item[] items = new Item[]{i1, i2};

			// Act
			DataProcessor.processData(items);

			// Assert
			int expectedRemainingSalesTarget = (3400 * 5) + (3500 * 1);
			assertEquals(expectedRemainingSalesTarget, DataProcessor.value);

		}
	}
	 
}
